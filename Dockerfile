FROM nginx:latest

ENV TZ Asia/Bangkok

VOLUME /var/log/nginx/log/
EXPOSE 80
